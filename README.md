# tuprivacidad.org

En este repositorio se encuentra el código fuente de la página web de TuPrivacidad.org.
La página estará alojada en GitLab pages.

Solo tiene HTLM y CSS3. Nada de Javascript. Tampoco tiene ningún rastreador ni ninguna plataforma de analíticas. Eres libre de usar la página para lo
que prefieras. Todas las contribuciones son bienvenidas.